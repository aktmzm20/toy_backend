package com.example.toyproject.product.service

import com.example.toyproject.common.BusinessCode
import com.example.toyproject.common.Code
import com.example.toyproject.product.dto.Product
import com.example.toyproject.product.repository.ProductRepository
import org.springframework.stereotype.Service

@Service
class ProductService (
    private val productRepository: ProductRepository)
{

    // 상품 정보 전체 조회
    fun getProdList(): List<Product> {
        return productRepository.findAll()
    }

    // 상품 정보 생성
    suspend fun createProductInfo (product : Product) : BusinessCode{
        val result = productRepository.createProductInfo(product);
        if (!result.prodId.isNullOrEmpty()){
            return BusinessCode(Code.ServiceError.API, Code.CodeName.SUCCESS);
        }else{
            return BusinessCode(Code.ServiceError.API, Code.CodeName.ERR_SERVER);
        }




    }









}