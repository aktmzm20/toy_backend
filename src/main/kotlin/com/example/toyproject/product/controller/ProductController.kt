package com.example.toyproject.product.controller

import com.example.toyproject.common.BusinessCode
import com.example.toyproject.common.Code
import com.example.toyproject.product.dto.Product
import com.example.toyproject.product.dto.ProductRequest
import com.example.toyproject.product.service.ProductService
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController

@RestController
@RequestMapping(value = arrayOf("/product"))
class ProductController (private val productService : ProductService){


    @GetMapping("/list")
    fun getProductList () : List<Product> {
        return  productService.getProdList();

    }

    @PostMapping( "/add")
    suspend fun createProduct (@RequestBody productRequest: ProductRequest) : BusinessCode {
        // prodRequest 를 내부 data class 로 변환 후 결과값을 BusinessCode 로 반환
        val BusinessCode = productService.createProductInfo(productRequest.toModel());
        return BusinessCode;
    }



    // 응답값 객체 변환
    private fun ProductRequest.toModel(): Product =
        Product(
            prodId = this.prodId,
            prodNm = this.prodNm,
            prodPaymentWait = this.prodPaymentWait,
            prodTypeCd = this.prodTypeCd ,
            prodSubNm = this.prodSubNm

        )

}