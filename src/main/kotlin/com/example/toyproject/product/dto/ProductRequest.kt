package com.example.toyproject.product.dto

data class ProductRequest(
    val prodNm : String,
    val prodId : String,
    val prodSerialNumber : Int,
    val prodTypeCd : String ,
    val prodSubNm: String?,
    val prodPaymentWait : Int? = 0
)
