package com.example.toyproject.product.dto

data class ProductResponse(
    val id: Long,
    val email: String,
    val name: String,
    val age: Int

)
