package com.example.toyproject.product.dto

import org.springframework.data.annotation.Id
import org.springframework.data.relational.core.mapping.Table

@Table(name = "tb_prod_info")
data class Product (
    @Id
    val idx: Long? = null,
    val prodId: String?,
    val prodNm: String?,
    val prodSubNm: String?,
    val prodTypeCd: String?,
    val prodPaymentWait : Int? = 0

)