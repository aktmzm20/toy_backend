package com.example.toyproject.product.repository

import com.example.toyproject.product.dto.Product
import org.springframework.data.jpa.repository.JpaRepository


interface ProductRepository : JpaRepository<Product, Long> {

    fun createProductInfo (product: Product) : Product

}