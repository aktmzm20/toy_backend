package com.example.cp3.common.util

import com.example.toyproject.common.BusinessCode
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.RequestBody

class ApiResponseUtil    {
    fun getResponseEntity ( businessCode: BusinessCode): ResponseEntity<*>{

        return ResponseEntity(businessCode, businessCode.id);
    }


}