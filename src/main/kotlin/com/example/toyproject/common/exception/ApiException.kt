package com.example.toyproject.common.exception

import com.example.toyproject.common.BusinessCode
import com.example.toyproject.common.Code


class ApiException : RuntimeException  {
    var error: BusinessCode? = null


    constructor() : super()

    constructor(msg: String) : super(msg)

    constructor(service: Code.ServiceError, codename: Code.CodeName) : super(codename.message) {
        this.error = BusinessCode(service, codename)
    }

    constructor(businessCode: BusinessCode) : super() {
        this.error = businessCode
    }

    constructor(service: Code.ServiceError, codename: Code.CodeName, obj: Any, errorMessage: String) : super(errorMessage) {
        this.error = BusinessCode(service, codename, obj, errorMessage)
    }

    constructor(service: Code.ServiceError, codename: Code.CodeName, errorMessage: String) : super(errorMessage) {
        this.error = BusinessCode(service, codename, errorMessage)
    }
}